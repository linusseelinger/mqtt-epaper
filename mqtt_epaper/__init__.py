#!/usr/bin/python
# -*- coding:utf-8 -*-
import sys
import os
import signal

picdir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
#libdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'lib')
#if os.path.exists(libdir):
#    sys.path.append(libdir)
import yaml

import logging
from waveshare_epd import epd7in5b_HD
from waveshare_epd import epd7in5_V2

from PIL import Image,ImageDraw,ImageFont
#import traceback
import threading
logging.basicConfig(level=logging.DEBUG)

from apscheduler.schedulers.blocking import BlockingScheduler

import time
import datetime

import paho.mqtt.client as mqtt

try:
  with open('/etc/mqtt-epaper.conf', 'r') as file:
    config = yaml.safe_load(file.read())
except:
  logging.error("Config file /etc/mqtt-epaper.conf not found!")
  exit(-1)

device_name = config['device_name']
topic_path = f"mqtt-epaper/{device_name}"

scheduler = BlockingScheduler(job_defaults={'misfire_grace_time': 1000})

two_colors = False
if config['epaper_type'] == 'epd7in5b_HD':
  epd = epd7in5b_HD.EPD()
  two_colors = True
elif config['epaper_type'] == 'epd7in5_V2':
  epd = epd7in5_V2.EPD()
else:
  logging.error("Unknown epaper_type set!")
  exit(-1)


push_messages = []


have_image = False



def display_content(epd):

    font48 = ImageFont.truetype(os.path.join(picdir, 'mqtt_epaper', 'Font.ttc'), 24)
    #font24 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 24)
    #font18 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 18)
    
    epd.init()
    TestImageRed = Image.new('1', (epd.width, epd.height), 255)  # 255: clear the frame
    TestImage = Image.new('1', (epd.width, epd.height), 255)  # 255: clear the frame
    
    draw_image_b = ImageDraw.Draw(TestImage)
    if two_colors:
      draw_image_r = ImageDraw.Draw(TestImageRed)
    else:
      draw_image_r = ImageDraw.Draw(TestImage)
    
    if (have_image):
        im = Image.open('/tmp/output.jpg')
        TestImage.paste(im)

    if (len(push_messages) > 0):
        total_height = 0
        #total_width = 0
        for push_message in push_messages:
            textsize = draw_image_b.textbbox((0,0), push_message[0], font = font48)
            total_height = total_height + textsize[3] + 40
            #total_width = max(total_width, textsize[2])

        textsize_first = draw_image_b.textbbox((0,0), push_messages[0][0], font = font48)
        
        center = [epd.width / 2, epd.height / 2 - total_height / 2 + textsize_first[3]]
        for push_message in push_messages:

            text = push_message[0]

            textsize = draw_image_b.textbbox((0,0), text, font = font48)
            text_top_left = (center[0] - textsize[2]/2, center[1] - textsize[3]/2)
           
            # First, draw white background. Then text.
            draw_image_b.rectangle((text_top_left[0] - 10, text_top_left[1] - 10, text_top_left[0] + textsize[2] + 10, text_top_left[1] + textsize[3] + 10), fill = 255, outline = 255, width = 10)
            draw_image_b.text(text_top_left, text, font = font48, fill=0)

            draw_image_r.rectangle((text_top_left[0] - 10, text_top_left[1] - 10, text_top_left[0] + textsize[2] + 10, text_top_left[1] + textsize[3] + 10), outline = 0, width=5)

            center[1] = center[1] + textsize[3] + 40

    logging.info("display")
    if two_colors:
      epd.display(epd.getbuffer(TestImage), epd.getbuffer(TestImageRed))
    else:
      epd.display(epd.getbuffer(TestImage))
    epd.sleep()
    logging.info("display sleeping")

content_changed = False
display_lock = threading.Lock()
def locked_display_update():
    global epd
    global content_changed
    global display_lock
    content_changed = True
    display_lock.acquire()
    if content_changed:
      content_changed = False
      display_content(epd)
    display_lock.release()

def request_display_update():
    global scheduler
    scheduler.add_job(locked_display_update, next_run_time=datetime.datetime.now())

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    add_message((f"Connected to MQTT server (Code {rc})", 60))

    client.subscribe(f"{topic_path}/#")

def remove_message(message):
    global push_messages
    if (message in push_messages):
        push_messages.remove(message)
        request_display_update()
        logging.info(f"Removed message {message[0]}")
    else:
        logging.warning(f"Tried to remove message {message[0]} which does not exist!")

def add_message(message):
    global push_messages
    push_messages.append(message)
    request_display_update()
    scheduler.add_job(lambda: remove_message(message), next_run_time=datetime.datetime.now() + datetime.timedelta(seconds=message[1]))
    logging.info(f"Added message {message[0]} for {message[1]} seconds")

def exit_main_loop():
    global scheduler
    scheduler.shutdown()

def sigterm_handler(_signo, _stack_frame):
    exit_main_loop()

last_image_hash = -1
def on_message(client, userdata, msg):
    global have_image
    global last_image_hash

    if (msg.topic == f"{topic_path}/command"):
        logging.info("Received message " + msg.topic + " " + msg.payload.decode('UTF-8'))
        payload_decoded = msg.payload.decode('UTF-8')
        if (payload_decoded == "exit"):
            exit_main_loop()
            return
        add_message((payload_decoded, 120))
    elif (msg.topic == f"{topic_path}/image"):
        new_image_hash = hash(msg.payload)
        if (new_image_hash != last_image_hash):
          last_image_hash = new_image_hash
          f = open('/tmp/output.jpg', "wb")
          f.write(msg.payload)
          f.close()
          have_image = True
          request_display_update()
        else:
          logging.info("Ignoring new image, has same hash as before")
    else:
        logging.warning("Unknown mqtt topic received!")



def clear_display():
    epd.init()
    epd.Clear()
    epd.sleep()


class MQTTEpaper(object):
    def run(self):
      try:
          signal.signal(signal.SIGTERM, sigterm_handler)


          mqtt_address = config['mqtt_server']
          logging.info("connecting to MQTT server")
          client = mqtt.Client(client_id=device_name, clean_session=False)
          client.on_connect = on_connect
          client.on_message = on_message

          client.connect(mqtt_address, 1883, keepalive=60)
          client.publish(f"{topic_path}/version", "0.1", retain=True)


          add_message((f"MQTT ePaper started, connecting to {mqtt_address}", 60))

          # Update screen every few hours in order to avoid burnin (recommended by waveshare)
          scheduler.add_job(request_display_update, 'interval', hours=12)

          client.publish(f"{topic_path}/screen_width", epd.width, retain=True)
          client.publish(f"{topic_path}/screen_height", epd.height, retain=True)
          client.loop_start()

          scheduler.start()

          client.loop_stop()

          logging.info("Shutdown cycle...")
          clear_display()
          epd.Dev_exit()

      except IOError as e:
          logging.info(e)

      except KeyboardInterrupt:
          logging.info("ctrl + c:")
          epd7in5b_HD.epdconfig.module_exit()
          exit()
